/*
Name: Nathaniel Mason
Login: nmason
Assignment Title: CIS 415 Project 1

This is my own work
*/

#include "p1fxns.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]){
    char buf[BUFSIZ] = "";
    char *p;
    int envval = -1;
    int opt;
    int fd = 0;

    if((p = getenv("USPS_QUANTUM_MSEC")) != NULL){
        envval = p1atoi(p);
    }

    while ((opt = getopt(argc, argv, "q:")) != -1){
        switch(opt) {
            case 'q': envval = atoi(optarg); break; // -q <quantum in msec> so optind will contain <quantum in msec>
        }
    }

    if(envval == -1){
        //printf("envval still -1\n");
    }

    FILE *file;
    while(optind < argc){
        //printf("Remaining arg: %s\n", argv[optind]);
        
        file = fopen(argv[optind], "r");
        if(file){
            //printf("Able to open file\n");
            fd = fileno(file);
        }
        else{
            //printf("Unable to open file\n");
        }
        optind++;
    }

    char myword[BUFSIZ] = "";
    char *args[100]; //array of strings with each string max length 100

    pid_t proc_id;
    int status;
    int numprograms = 0;

    //Read commands and arguments from fd, where fd can be stdin or a file
    while(p1getline(fd, buf, BUFSIZ) != 0){
        int j = 0;
        int i = 0;

        while(i != -1){ // i will equal -1 if at end of buffer
            if(i != 0){
                args[j] = malloc(100);
                p1strcpy(args[j], myword);
                
                //printf("Just added: %s at position %d\n", myword, j);
                j++;
            }
            
            i = p1getword(buf, i, myword);
            int slen = p1strlen(myword);

            if(myword[slen - 1] == '\n'){
                //printf("edited newline char\n");
                myword[slen - 1] = '\0';
            }
        }
        args[j] = NULL;
        
        /*
        for(int n = 0; n < j; n++){
            printf("ARG: '%s'\n", args[n]);
        }
        */
        
        proc_id = fork();
        
        switch(proc_id){
            case -1:
                printf("Fork error occurred\n");
                break;
            case 0:
                execvp(args[0], args);
                fprintf(stderr, "Child could not execute program: %s\n", args[0]);
                for(int z = 0; z < j; z++){
                    free(args[z]);
                }
                fclose(file);
                return EXIT_FAILURE;
            default:
                //printf("parent process executing code\n");
                break;
        }
        
        numprograms++;
        
        for(int z = 0; z < j; z++){
            free(args[z]);
        }
    }

    //printf("Reached end of file, no more lines to get\n");
    if(fd != 0){
        fclose(file);
    }

    for(int x = 0; x < numprograms; x++){
        wait(&status);
        /*
        if(WIFEXITED(status)){
            printf("child %d exited successfully\n", x);
        }
        if(WIFSIGNALED(status)){
            printf("child %d exited with signal\n", x);
        }
        */
    }
}
/*
Name: Nathaniel Mason
DuckID: nmason
Assignment Title: CIS 415 Project 1

This is my own work except that I used the tip that Troy Clendenen mentioned (using sleep(1)) on slack to allow valgrind to work
*/

#include "p1fxns.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define UNUSED __attribute__ ((unused))

typedef struct pcb { //based this code on signal.c code from lab
    pid_t pid;
} PCB;

int usr1 = 0;
PCB pidarray[512];

void callevp(UNUSED int sig){
    //printf("received signal SIGUSR1 and am doing execvp\n");
    usr1++;
}

int main(int argc, char *argv[]){
    char buf[BUFSIZ] = "";
    char *p;
    int envval = -1;
    int opt;
    int fd = 0;

    if(signal(SIGUSR1, callevp) == SIG_ERR){
        printf("Signal error recieved\n");
        return EXIT_FAILURE;
    }

    if((p = getenv("USPS_QUANTUM_MSEC")) != NULL){
        envval = p1atoi(p);
    }

    while ((opt = getopt(argc, argv, "q:")) != -1){
        switch(opt) {
            case 'q': envval = atoi(optarg); break; // -q <quantum in msec> so optind will contain <quantum in msec>
        }
    }

    if(envval == -1){
        //printf("envval still -1\n");
    }

    FILE *file;
    while(optind < argc){
        //printf("Remaining arg: %s\n", argv[optind]);
        
        file = fopen(argv[optind], "r");
        if(file){
            //printf("Able to open file\n");
            fd = fileno(file);
        }
        else{
            //printf("Unable to open file\n");
        }
        optind++;
    }

    char myword[BUFSIZ] = "";
    char *args[100]; //array of strings with each string max length 100

    pid_t proc_id;
    int status;
    int numprograms = 0;

    //Read commands and arguments from fd, where fd can be stdin or a file
    while(p1getline(fd, buf, BUFSIZ) != 0){
        int j = 0;
        int i = 0;

        while(i != -1){ // i will equal -1 if at end of buffer
            if(i != 0){
                args[j] = malloc(100);
                p1strcpy(args[j], myword);
                
                //printf("Just added: %s at position %d\n", myword, j);
                j++;
            }
            
            i = p1getword(buf, i, myword);
            int slen = p1strlen(myword);

            if(myword[slen - 1] == '\n'){
                //printf("edited newline char\n");
                myword[slen - 1] = '\0';
            }
        }
        args[j] = NULL;
        
        for(int n = 0; args[n] != NULL; n++){
            //printf("ARG: %s\n", args[n]);
        }
        
        PCB *p = pidarray + numprograms;
        
        proc_id = fork();
        //int evpresult = 0;
        
        switch(proc_id){
            case -1:
                printf("Fork error occurred\n");
                break;
            case 0:
                //Wait on SIGUSR1 signal, then call evp function which will invoke execvp
                //printf("waiting on SIGUSR1 signal...\n");
                while(!usr1){
                    pause(); //process waits for signal
                }
                execvp(args[0], &args[0]);
                fprintf(stderr, "Child could not execute process: %s\n", args[0]);
                for(int z = 0; z < j; z++){
                    free(args[z]);
                }
                fclose(file);
                return EXIT_FAILURE;
            default:
                //printf("parent process executing code\n");
                sleep(1);
                p->pid = proc_id; //assign process-id of child to p->pid (parent proc_id is child's process-id)
                break;
        }
        
        numprograms++;
        
        for(int z = 0; z < j; z++){
            free(args[z]);
        }
    }

    //printf("numprograms: %d\n", numprograms);
    
    //printf("Reached end of file, no more lines to get\n");

    for(int x = 0; x < numprograms; x++){
        //printf("Process id: %d\n", pidarray[x].pid);
        //printf("sending usr1 signal...\n");
        kill(pidarray[x].pid, SIGUSR1); //send SIGUSR1 signal for each child process
    }
    
    for(int z = 0; z < numprograms; z++){
        //printf("suspending...\n");
        //printf("Stopping Process id: %d\n", pidarray[z].pid);
        kill(pidarray[z].pid, SIGSTOP); //send SIGSTOP signal for each child process
    }

    //sleep(10); //allows for seeing stopped programs in top
    for(int a = 0; a < numprograms; a++){
        //printf("continuing...\n");
        kill(pidarray[a].pid, SIGCONT); //send SIGCONT signal for each child process
    }
    
    for(int b = 0; b < numprograms; b++){
        //printf("waiting...\n");
        wait(&status);
        
        /*
        if(WIFEXITED(status)){
            printf("child %d exited successfully\n", b);
        }
        if(WIFSIGNALED(status)){
            printf("child %d exited with signal\n", b);
        }
        */
    }

    if(fd != 0){
        fclose(file);
    }
    return 0;
}
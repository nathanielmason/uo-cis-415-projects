/*
Name: Nathaniel Mason
DuckID: nmason
Assignment Title: CIS 415 Project 1

This is my own work except that I talked about some of the code structure of the child handler with Chris Cortes
*/

#include "p1fxns.h"
#include "ADTs/queue.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdbool.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <string.h>

#define UNUSED __attribute__ ((unused))
#define MAX 128
#define MAXBUF 4096
#define MIN_QUANTUM 20
#define MAX_QUANTUM 2000
#define MS_PER_TICK 20

typedef struct pcb { //based this code on signal.c and roundrobin.c code from lab
    pid_t pid;
    int ticks;
    bool isalive;
    bool usr1;
} PCB;

int numprograms = 0;
volatile int active_processes = 0;
volatile int usr1 = 0;
PCB pidarray[MAX];
const Queue *q = NULL;
PCB *current = NULL;
int ticks_in_quantum = 0;

static int pid2index(pid_t pid){
    int i;
    for(i = 0; i < numprograms; i++){
        if(pidarray[i].pid == pid){
            return i;
        }
    }
    return -1;
}

static void chld_handler(UNUSED int sig){
    //pid_t pid;
    
    pid_t chldpid = 0;
    int chldstatus = 0;
    
    //printf("child handler being called\n");

    while ((chldpid = waitpid(-1, &chldstatus, WNOHANG)) > 0){
        if(WIFEXITED(chldstatus) || WIFSIGNALED(chldstatus)) {
            //if(active_processes > 0){
                active_processes--;
                //printf("#active processes: %d\n", active_processes);
                int pidind = pid2index(chldpid);
                pidarray[pidind].isalive = false;
            //}
            
            fprintf(stderr, "Child with PID: %d has exited\n", chldpid);
        }
    }
}

static void alrm_handler(UNUSED int sig) {
    //printf("SIGALRM RECEIVED\n");
    
    if(current != NULL){
        //printf("CURRENT pid: %d\n", current->pid);
        //printf("Current USR1: %d\n", current->usr1);
        if(current->isalive){
            //printf("current is alive and current ticks: %d\n", current->ticks);
            --current->ticks;
            if(current->ticks > 0){
                return;
            }
            //printf("sending SIGSTOP...\n");
            kill(current->pid, SIGSTOP); //no more time left for this process, send STOP signal
            //printf("ENQUEUEING ADT_VALUE of still alive process: %p\n", ADT_VALUE(current));
            q->enqueue(q, ADT_VALUE(current)); //enqueue PCB onto queue
        }
        current = NULL;
    }
    while(q->dequeue(q, ADT_ADDRESS(&current))){
        //printf("dequeuing...\n");
        //printf("Dequeued address is: %p\n", current);
        //printf("Dequeued pid is: %d and is it alive? %d\n", current->pid, current->isalive);

        if(!current->isalive){ //if not alive, dequeue another PCB
            //printf("current is not alive so continuing...\n");
            continue;
        }
        current->ticks = ticks_in_quantum;
        //printf("current ticks are: %d\n", current->ticks);
        if(current->usr1){
            //printf("sending SIGUSR1...\n");
            current->usr1 = false; //first time, so send USR1 signal
            kill(current->pid, SIGUSR1);
        }
        else{
            //printf("sending SIGCONT...\n");
            kill(current->pid, SIGCONT); //not the first time, so send CONT signal
        }
        return;
    }
    
}

void callevp(UNUSED int sig){
    //printf("received signal SIGUSR1 and am doing execvp\n");
    usr1++;
}

int main(int argc, char *argv[]){
    char buf[BUFSIZ] = "";
    char *p;
    int quantum = 0;
    int opt;
    int fd = 0;
    struct itimerval interval;

    if(signal(SIGUSR1, callevp) == SIG_ERR){
        fprintf(stderr, "Error creating SIGUSR1 handler\n");
        return EXIT_FAILURE;
    }

    if(signal(SIGALRM, alrm_handler) == SIG_ERR){
        fprintf(stderr, "Error creating SIGALRM handler\n");
        return EXIT_FAILURE;
    }

    
    if(signal(SIGCHLD, chld_handler) == SIG_ERR){
        fprintf(stderr, "Error creating SIGCHLD handler\n");
        return EXIT_FAILURE;
    }
    

    if((p = getenv("USPS_QUANTUM_MSEC")) != NULL){
        quantum = p1atoi(p);
    }

    while ((opt = getopt(argc, argv, "q:")) != -1){
        switch(opt) {
            case 'q': quantum = atoi(optarg); break; // -q <quantum in msec> so optind will contain <quantum in msec>
        }
    }

    if(quantum < MIN_QUANTUM || quantum > MAX_QUANTUM) {
        printf("Quantum value is outside minimum or maximum allowed values\n");
        return 1;
    }

    quantum = MS_PER_TICK * ((quantum + 1) / MS_PER_TICK);
    ticks_in_quantum = quantum / MS_PER_TICK;

    /*
    if(quantum == 0){
        printf("quantum still 0\n");
    }
    else {
        printf("quantum is: %d\n", quantum);
    }
    */

    FILE *file;
    while(optind < argc){
        //printf("Remaining arg: %s\n", argv[optind]);
        
        file = fopen(argv[optind], "r");
        if(file){
            //printf("Able to open file\n");
            fd = fileno(file);
        }
        else{
            //printf("Unable to open file\n");
        }
        optind++;
    }

    q = Queue_create(doNothing);
    char myword[BUFSIZ] = "";
    char *args[100]; //array of strings with each string max length 100

    pid_t proc_id;
    //int status;

    //Read commands and arguments from fd, where fd can be stdin or a file
    while(p1getline(fd, buf, BUFSIZ) != 0){
        int j = 0;
        int i = 0;

        while(i != -1){ // i will equal -1 if at end of buffer
            if(i != 0){
                args[j] = malloc(100);
                p1strcpy(args[j], myword);
                
                //printf("Just added: %s at position %d\n", myword, j);
                j++;
            }
            
            i = p1getword(buf, i, myword);
            int slen = p1strlen(myword);

            if(myword[slen - 1] == '\n'){
                //printf("edited newline char\n");
                myword[slen - 1] = '\0';
            }
        }
        args[j] = NULL;
        
        /*
        for(int n = 0; args[n] != NULL; n++){
            printf("ARG: %s\n", args[n]);
        }
        */
        
        PCB *p = pidarray + numprograms;
        
        proc_id = fork();
        //int evpresult = 0;
        
        switch(proc_id){
            case -1:
                printf("Fork error occurred\n");
                break;
            case 0:
                //Wait on SIGUSR1 signal, then call evp function which will invoke execvp
                //printf("waiting on SIGUSR1 signal...\n");
                while(!usr1){
                    pause(); //process waits for signal
                }
                
                execvp(args[0], &args[0]);
                fprintf(stderr, "Child could not execute program: %s\n", args[0]);
                for(int z = 0; z < j; z++){
                    free(args[z]);
                }
                goto cleanup;
            default:
                //printf("parent process executing code\n");
                sleep(1);
                p->pid = proc_id; //assign process-id of child to p->pid (parent proc_id is child's process-id)
                p->isalive = true;
                p->usr1 = true;
                //printf("ENQUEUING ADT_VALUE: %p\n", ADT_VALUE(p));
                q->enqueue(q, ADT_VALUE(p));
                break;
        }
        
        numprograms++;
        
        for(int z = 0; z < j; z++){
            free(args[z]);
        }
    }

    active_processes = numprograms;

    interval.it_value.tv_sec = MS_PER_TICK/1000;//s
    interval.it_value.tv_usec = (MS_PER_TICK*1000) % 1000000;//ms
    interval.it_interval = interval.it_value;

    if(setitimer(ITIMER_REAL, &interval, NULL) == -1) { //timer sends SIGALRM once time runs out, then timer restarts
        printf("Timer set error occurred\n");
        for(int i = 0; i < active_processes; i++){
            kill(pidarray[i].pid, SIGKILL);
        }
        goto cleanup;
    }    
    
    while(active_processes > 0){
        pause();
    }
    
    //alrm_handler(SIGALRM);

    //printf("Reached end of file, no more lines to get\n");
    
    cleanup:
        if(fd != 0){
            fclose(file);
        }
        q->destroy(q);
        return 0;
}
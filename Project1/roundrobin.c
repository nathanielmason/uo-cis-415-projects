//Timmy Tang
//
//Round robin scheduling
//
//
#include "ADTs/queue.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <stdbool.h>
#include <string.h>

#define UNUSED __attribute__((unused))
#define MAX 128
#define MAXBUF 4096
#define MIN_QUANTUM 20
#define MAX_QUANTUM 2000
#define MS_PER_TICK 20

#define UNUSED __attribute__((unused))

typedef struct pcb {
	pid_t pid;
	int ticks;
	bool isalive;
	bool usr1;
} PCB;

PCB array[MAX];
int num_procs = 0;
volatile int active_processes = 0;
volatile int usr1 = 0;
pid_t parent;
const Queue *q = NULL;
PCB *current = NULL;
int ticks_in_quantum = 0;

static int pid2index(pid_t pid) {
	int i;
	for(i = 0; i < num_procs; ++i) {
		if(array[i].pid == pid)
			return 	i;
	}
	return -1;

}

static void usr1_handler(UNUSED int sig) {
	usr1++;
}

static void usr2_handler(UNUSED int sig) {
}

static void chld_handler(UNUSED int sig) {//We do not send this signal. 
	//TODO
	//
	//PID
	//pid = waitpid(-1, status, WNOHANG) 
	//status of the process is actually dead
	//	active_processes--;
	//	array[pid].isalive = false
	//	signal(parent, USR2)
	pid_t pid;

}
static void alrm_handler(UNUSED int sig) {
	//TODO
	//if current is not null, AKA we have a process lined up
	//	if it's alive:
	//		we decrement the time
	//		if there is still time:
	//			it can still run. Return;
	//		send signal to stop the process
	//		enqueue the process
	//	set current to NULL
	//while we can dequeue stuff: dequeue and set current to that
	//	if current is not alive:
	//		continue
	//	set current time
	//	if current has seen usr1
	//		current->usr1 = false
	//		call usr1 signal on process
	//	else
	//		call sigcont on process
	//	return;
	if(current != NULL) {
		if(current->isalive) {
			--current->ticks;
			if(current->ticks > 0) {
				return;
			}
			kill(current->pid, SIGSTOP);
			q->enqueue(q, ADT_VALUE(current));
		}
		current = NULL;
	}
	while(q->dequeue(q, ADT_ADDRESS(&current))) {
		if(!current->isalive)
			continue;
		current->ticks = ticks_in_quantum;
		if(current->usr1) {
			current->usr1 = false;
			kill(current->pid, SIGUSR1);
		} else {
			kill(current->pid, SIGCONT);
		}
	}
	return;
}

int main(int argc, char * argv[]) {
	int quantum = 0;
	char * filename = NULL;
	FILE * wfd;
	struct itimerval interval;

	if(argc != 3) {
		printf("Wrong number of args\n");	
	}
	quantum = atoi(argv[1]);

	if(quantum < MIN_QUANTUM || quantum > MAX_QUANTUM) {
		printf("Unreasonable quantum\n");
		return 1;
	}
	filename = argv[2];
	wfd = fopen(filename, "r");

	signal(SIGUSR1, usr1_handler);
	signal(SIGUSR2, usr2_handler);
	parent = getpid();

	signal(SIGCHLD, chld_handler);
	signal(SIGALRM, alrm_handler);
	
	quantum = MS_PER_TICK * ((quantum + 1) / MS_PER_TICK);
	ticks_in_quantum = quantum / MS_PER_TICK;
	
	q = Queue_create(doNothing);
	int n = 0;
	size_t line_size = 0;
	char * buffer = NULL;
	while((n = getline(&buffer, &line_size, wfd)) != -1) {
		pid_t pid;
		PCB * p = array+num_procs;
		switch(pid=fork()) {
			case -1:
				break;
			case 0:
				while(!usr1)
					pause();
				printf("%s\n", buffer);	
				break;
			default:
				p->pid = pid;
				p->isalive = true;
				p->usr1 = true;
				q->enqueue(q, ADT_VALUE(p));
				break;
		}
		num_procs++;
	}

	active_processes = num_procs;
	
	interval.it_value.tv_sec = MS_PER_TICK/1000;//seconds
	interval.it_value.tv_usec = (MS_PER_TICK*1000) % 1000000;//micrseconds
	interval.it_interval = interval.it_value;
	if(setitimer(ITIMER_REAL, &interval, NULL) == -1) {
		printf("bad timer\n");
		for(int i = 0; i < num_procs; ++i)
			kill(array[i].pid, SIGKILL);
		goto cleanup;
	}
	
	
	alrm_handler(SIGALRM);	

	/*
	while(active_processes > 0)
		pause();
	*/
cleanup:
	free(buffer);
	buffer = NULL;
	fclose(wfd);
	q->destroy(q);
	return 0;
}

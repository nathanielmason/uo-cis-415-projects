// Name: Nathaniel Mason
// Login: nmason
// Assignment Title: CIS 415 Project 2
// This is my own work, except that I discussed how the overall design of the threads should work
// in the project with Chris Cortes

#include "BoundedBuffer.h"
#include "diagnostics.h"
#include "packetdriver.h"
#include "fakeapplications.h"
#include "queue.h"
#include "freepacketdescriptorstore__full.h"
#include "networkdevice__full.h"
#include <pthread.h>
#include <stdio.h>

pthread_t sendingThread, receivingThread;

NetworkDevice **mynd_ptr;
BoundedBuffer *BB_arr[MAX_PID]; //to store incoming pds, one boundedbuffer for each pid
BoundedBuffer *outgoing_pds; //to store outgoing pds, then these will be requested to go to the network driver one at a time
FreePacketDescriptorStore *myfpds;

int outgoing_pds_ready = 0;

//Define functions for threads, so will need two functions
void *sending(void *args){
    //retrieve one pd at a time from queue and request network driver to transmit packet
    //printf("running sending thread\n");
    PacketDescriptor *outgoingpd;
    NetworkDevice *mynd = (NetworkDevice *)args;
    int outresult = myfpds->nonblockingGet(myfpds, &outgoingpd); //get pd from fpds

    if(outresult == 0){
        printf("getting outgoingpd from fpds failed\n");
    }
    else{
        initPD(outgoingpd);

        if(outgoing_pds_ready){
            int read_result = outgoing_pds->nonblockingRead(outgoing_pds, (void **)&outgoingpd); //will return 0 if queue is empty
            if(read_result == 0){
                printf("nonblockRead on outgoingpds failed\n");
            }
            //NetworkDevice *mynd = *mynd_ptr;

            while(read_result != 0){
                printf("attempting to send outgoingpd...\n");
                if(mynd->sendPacket(mynd, outgoingpd) == 0){ //could not send, try one more time
                    if(mynd->sendPacket(mynd, outgoingpd) == 0){
                        printf("Failed to send packet to network after trying twice\n");
                    }
                }
                initPD(outgoingpd); //reset pd
                read_result = outgoing_pds->nonblockingRead(outgoing_pds, (void **)&outgoingpd); //retrieve next pd from queue
            }

            myfpds->blockingPut(myfpds, outgoingpd); //put pd back in fpds
        }
        else{
            printf("nothing in outgoing_pds yet\n");
        }
    }
    return NULL;
}

void *receiving(void *args){
    //retrieve pd from store, initilaize, register with network driver, then wait for msg
    //once packet is received, queue pd for receipt by destination application

    //printf("Running receiving thread\n");
    
    NetworkDevice *nd = (NetworkDevice *)args;
    PacketDescriptor *mypd;
    int myresult;

    pd_prep:

    //printf("myfpds size: %ld\n", (long unsigned int)myfpds->size);
    myresult = myfpds->nonblockingGet(myfpds, &mypd);
    if(myresult == 0){
        printf("myfpds nonblockingGet has failed\n");
        goto pd_prep;
    }

    else{
        initPD(mypd); //reset descriptor before registering

        //printf("Registering pd...\n");
        nd->registerPD(nd, mypd);
        //printf("Awaiting incoming pd...\n");
        nd->awaitIncomingPacket(nd); //blocks thread until registered pd has been filled with packet

        //printf("Received packet from network!\n");
        PID arr_ind = getPID(mypd);
        //printf("PD arr_ind: %ld\n", (long unsigned int)arr_ind);
        BB_arr[arr_ind]->blockingWrite(BB_arr[arr_ind], mypd); //queue pd for receipt by application
        //printf("Wrote to BB_arr for %ld\n", (long unsigned int)arr_ind);
        
        if(myfpds->size == 0){
            printf("myfpds empty\n");
            return NULL;
        }
        else{
            goto pd_prep;
        }
    }
}

void init_packet_driver(NetworkDevice *nd, void *mem_start, unsigned long mem_length, FreePacketDescriptorStore **fpds_ptr){
    myfpds = FreePacketDescriptorStore_create(mem_start, mem_length);
    //printf("myfpds size: %ld\n", (long unsigned int)myfpds->size);
    mynd_ptr = &nd; //assign nd to global variable mynd_ptr

    //Need 10 buffers because there are 10 possible PIDs
    long unsigned int fpds_size = (long unsigned int)myfpds->size;

    for(int i = 0; i <= 10; i++){
        BB_arr[i] = BoundedBuffer_create(fpds_size);
    }

    outgoing_pds = BoundedBuffer_create(fpds_size);
    //printf("BoundedBuffers have been created\n");

    void *args = (void*)nd;
    
    //need sending thread and receiving thread
    pthread_create(&sendingThread, NULL, sending, args);
    
    pthread_create(&receivingThread, NULL, receiving, args);

    *fpds_ptr = myfpds;
    //printf("fpds created\n");
}

void blocking_send_packet(PacketDescriptor *pd){
    //printf("Running blocking_send_packet\n");
    outgoing_pds->blockingWrite(outgoing_pds, (void *)pd);
    outgoing_pds_ready = 1; //once something written to outgoing_pds, then thread starts
    
    return;
}

int nonblocking_send_packet(PacketDescriptor *pd){
    //printf("Running nonblocking_send_packet\n");
    int result = outgoing_pds->nonblockingWrite(outgoing_pds, (void *)pd);
    
    if(result == 1){
        outgoing_pds_ready = 1;
    }
    return result;
}

void blocking_get_packet(PacketDescriptor **pd, PID pid){
    //printf("Running blocking_get_packet\n");
    //printf("Reading least recent pd from queue\n");
    BB_arr[pid]->blockingRead(BB_arr[pid], (void**)pd); //get least recently received pd for this pid
}

int nonblocking_get_packet(PacketDescriptor **pd, PID pid){
    //printf("Running nonblocking_get_packet\n");
    
    return BB_arr[pid]->nonblockingRead(BB_arr[pid], (void**)pd);
}
/*
Name: Nathaniel Mason
DuckID: nmason
Assignment Title: CIS 415 Project 3
This is my own work except that I got some advice on how to process the repeat event from some fellow students that are part of a UO cis discord server
(since I only saw their usernames in the server, I do not know what their full names are so I can't able to list them here)
*/

#include "BXP/bxp.h"
#include "ADTs/queue.h"
#include "ADTs/cskmap.h"
#include "ADTs/prioqueue.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <valgrind/valgrind.h>

#define USECS (1000 * 1000) // 1000 microseconds in 1 millisecond

#define UNUSED __attribute__((unused))
#define HOST "localhost" 
#define PORT 19999
#define SERVICE "DTS"

const Queue *q = NULL;
const Queue *rep_q = NULL;
const PrioQueue *pq = NULL;
const CSKMap *m = NULL;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t insertion_finished = PTHREAD_COND_INITIALIZER;
pthread_cond_t retrieve_pqentries = PTHREAD_COND_INITIALIZER;

typedef struct event {
    char eventargs[10][100]; //array of 10 strings of max length 100
} Event;



// 2 threads required for this part
// Need prio queue, as well as a list of queries,, use map for this. Each event will have an id which will allow each event to be
//accessed in the map, also need compare function for prio queue and for map. Map also needs hash function, use hashmap from adts
void *timer(){
    struct timeval now;
    
    //printf("joined timer fxn\n");
    //int printed = 1;
    
    struct timeval *initial_retrieved_time_p;
    Event *initial_retrieved_event;

    while(1) {
        usleep(USECS);
        pthread_mutex_lock(&mutex);
        gettimeofday(&now, NULL); // this is the current time
        //printf("retrieve items from prio queue, then print items\n");
        
        unsigned long long curr_total_time = (now.tv_sec * 1000000) + now.tv_usec;
        // get all entries from pq that or <= curr_total_time

        int pq_size;
        pq_size = pq->size(pq);

        //printf("pq size: %d\n", pq_size);
        
        initial_retrieved_time_p = (struct timeval *)malloc(sizeof(struct timeval));
        initial_retrieved_event = (Event *)malloc(sizeof(Event));

        if(pq_size > 0){
            /*
            prioval = *prio;
            printf("prioval: %llu\n", prioval);
            
            char *first_arg = retrieved_event->eventargs[0];
            printf("first_arg: %s\n", first_arg);
            
            char *second_arg = retrieved_event->eventargs[1];
            printf("second_arg: %s\n", second_arg);

            char *server_id = retrieved_event->eventargs[7];
            printf("server id: %s\n", server_id);
            */
            
            int removal_worked = 1;
            //unsigned long long *prio;
            unsigned long long prioval;

            removal_worked = pq->removeMin(pq, (void**)&initial_retrieved_time_p, (void *)&initial_retrieved_event);
            prioval = (1000000*(unsigned long long)initial_retrieved_time_p->tv_sec) + (unsigned long long)initial_retrieved_time_p->tv_usec;
            //printf("Initial prioval: %llu\n", prioval);
            int first_time = 1;

            while((prioval <= curr_total_time)){ 
                if(first_time){
                    //printf("current total time: %llu\n", curr_total_time);

                    //printf("enqueing...\n");
                    q->enqueue(q, (void *)initial_retrieved_event);
                    free(initial_retrieved_time_p);
                    //printf("enqueued event with svid %s\n", initial_retrieved_event->eventargs[7]);
                    first_time = 0;
                }

                else{
                    struct timeval *retrieved_time_p = (struct timeval *)malloc(sizeof(struct timeval));
                    Event *retrieved_event = (Event *)malloc(sizeof(Event));
                    removal_worked = pq->removeMin(pq, (void**)&retrieved_time_p, (void *)&retrieved_event);
                    //printf("starting prioval: %llu\n", prioval);
                    //printf("current total time: %llu\n", curr_total_time);
                    
                    if(removal_worked){
                        //printf("removal successful\n");
                        prioval = (1000000*(unsigned long long)retrieved_time_p->tv_sec) + (unsigned long long)retrieved_time_p->tv_usec;
                        //printf("updated prioval: %llu\n", prioval);
                        //printf("enqueing...\n");
                        q->enqueue(q, (void *)retrieved_event);
                        //printf("enqueued event with svid %s\n", retrieved_event->eventargs[7]);
                    }
                    else{
                        prioval = curr_total_time + 1;
                    }                    
                }
            }
            
        }
        
        Event *queue_event = (Event *)malloc(sizeof(Event));
        
        //not dealing with prioqueue anymore, now just go through events in queue do appr. actions
        //check if queue not empty

        while(q->dequeue(q, (void *)&queue_event) != 0){
            char retrieved_svid[20];
            char clid_str[20];
            char *str_ptr;
            long clid;

            strcpy(clid_str, queue_event->eventargs[1]);
            //printf("retrieved clid: %s\n", clid_str);
            clid = strtol(clid_str, &str_ptr, 10);

            char event_name[20];
            strcpy(event_name, queue_event->eventargs[0]);
            char *rep = "Repeat";
            //printf("event name: %s\n", event_name);

            if(strcmp(event_name, rep) == 0){ //Event is Repeat
                //printf("event is repeat\n");
                char *reps_ptr;
                long repeats = strtol(queue_event->eventargs[3], &reps_ptr, 10);
                //printf("repeats: %ld\n", repeats);

                if((repeats > 0)){ //keep repeating until repeats == 1
                    printf("Event fired: %lu|%s|%s|%s\n", clid, queue_event->eventargs[4], queue_event->eventargs[5], queue_event->eventargs[6]);
                    if(repeats != 1){
                        repeats -= 1;
                        char updated_repeats_str[20];
                        sprintf(updated_repeats_str, "%lu", repeats);
                        strcpy(queue_event->eventargs[3], updated_repeats_str);
                        //printf("updated repeats: %s\n", queue_event->eventargs[3]);
                    }
                    else if(repeats == 1){
                        repeats = -1;
                        free(queue_event);
                    }
                    
                }
                if(repeats == 0){ //infinitely repeat
                    printf("Event fired: %lu|%s|%s|%s\n", clid, queue_event->eventargs[4], queue_event->eventargs[5], queue_event->eventargs[6]);
                }

                if(repeats != -1){
                    //update priority and put back in priority queue
                    char *ms_ptr;
                    long millisecs = strtol(queue_event->eventargs[2], &ms_ptr, 10);
                    long usecs_interval = millisecs * 1000;
                    struct timeval *start_repeat_time_p = (struct timeval *)malloc(sizeof(struct timeval));
                    gettimeofday(start_repeat_time_p, NULL);
                    //unsigned long start_repeat_total_time = (start_repeat_time.tv_sec * 1000000) + start_repeat_time.tv_usec;
                    start_repeat_time_p->tv_usec += usecs_interval; 

                    //printf("inserting event back into pq with updated prioirty based on interval\n");
                    pq->insert(pq, (void *)start_repeat_time_p, (void *)queue_event); //put back into pq with new priority that incorporates the interval in ms
                }
            }

            else{ //event is OneShot
                strcpy(retrieved_svid, queue_event->eventargs[7]);
                //printf("retrieved svid: %s\n", retrieved_svid);

                if(m->containsKey(m, retrieved_svid)){
                    //printf("event %s needs to be canceled so will be freed\n", retrieved_svid);
                }
                else{
                    printf("Event fired: %lu|%s|%s|%s\n", clid, queue_event->eventargs[4], queue_event->eventargs[5], queue_event->eventargs[6]);
                }
                free(queue_event);
                
            }
            
        }

        
        //printf("\n");
        VALGRIND_MONITOR_COMMAND("leak_check summary");
        
        pthread_mutex_unlock(&mutex);
    }
    
}

int extractWords(char *buf, char *sep, char *words[]) {
    int i;
    char *p;

    for (p = strtok(buf, sep), i = 0; p != NULL; p = strtok(NULL, sep),i++)
        words[i] = p;
    words[i] = NULL;
    return i;
}

void *receiver_fxn () {
    //printf("joined receiver fxn\n");
    BXPEndpoint ep;
    BXPService svc;
    
    int ifEncrypt = 1;
    char query[10000], response[10001];
    unsigned qlen, rlen;

    assert(bxp_init(PORT, ifEncrypt));
    assert((svc = bxp_offer(SERVICE)));

    //check the request received for legal strings and number of arguments
    //Legal queries have secitons split using | so can split each string by the | character
    char *delim = "|";
    //char *qtoken;
    int one_shot_req;
    int repeat_req;
    int cancel_req;
    int num_args_needed;
    int num_args_satisfied;
    int num_args;
    char *word1 = "OneShot";
    char *word2 = "Repeat";
    char *word3 = "Cancel";
    char query_to_split[10000];

    unsigned long svid = 0;

    char *args[10];
    int count = 0;
    
    while ((qlen = bxp_query(svc, &ep, query, 10000)) > 0) {
        //printf("Query: %s\n", query);
        one_shot_req = 0;
        repeat_req = 0;
        cancel_req = 0;
        num_args = 0; 
        num_args_satisfied = 0;
        Event *event_pointer = NULL;
        
        svid += 1; //each event will have a unique svid
        char arg_svid[20];
        sprintf(arg_svid, "%lu", svid);

        //printf("query svid: %s\n\n", arg_svid);

        strcpy(query_to_split, query);
        //printf("query to split: %s\n", query_to_split);

        num_args = extractWords(query_to_split, delim, args);
        
        /*
        unsigned long val = 10;
        
        void *test = (void *)&val;
        unsigned long *printvalp = (unsigned long *)test;
        unsigned long printval = *printvalp;
        printf("test: %lu\n", printval);
        */

        char *first_word = args[0];
        
        if(strcmp(first_word, word1) == 0){
            one_shot_req = 1;
        }
        if(strcmp(first_word, word2) == 0){
            repeat_req = 1;
        }

        if(strcmp(first_word, word3) == 0){
            cancel_req = 1;
        }

        if(one_shot_req || repeat_req){
            num_args += 1;
            event_pointer = (Event *)malloc(sizeof(Event));

            args[7] = arg_svid;
            //printf("num args updated: %d\n", num_args);
            
            for(int i = 0; i < num_args; i++){
                //printf("arg %d: %s\n", i, args[i]);
                strcpy(event_pointer->eventargs[i], args[i]);
                //printf("e->eventargs %d: %s\n", i, e->eventargs[i]);
            }
            num_args_needed = 8; //8 because also have server id now
        }
        else if(cancel_req){
            num_args_needed = 2;
        }

        if(num_args == num_args_needed){
            num_args_satisfied = 1;
        }
        
        int cancel_val = 0;
        int put_result;
        
        if((one_shot_req || repeat_req) && num_args_satisfied){
            struct timeval *event_time = (struct timeval *)malloc(sizeof(struct timeval));
            gettimeofday(event_time, NULL);

            /*
            unsigned long long ev_secs = (unsigned long long)event_time->tv_sec;
            unsigned long long ev_usecs = (unsigned long long)event_time->tv_usec;
            unsigned long long total_time_usecs = (1000000*event_time->tv_sec) + event_time->tv_usec;
            printf("ev_secs: %llu\n", ev_secs);
            printf("ev_usecs: %llu\n", ev_usecs);
            printf("query's total time: %llu\n\n", total_time_usecs);
            */

            sprintf(response, "1%08lu", svid);
            
            pq->insert(pq, (void *)event_time, (void *)event_pointer);
            count += 1;
            //printf("event time and event were inserted into pq\n");
        }
        else if((cancel_req) && num_args_satisfied){
            cancel_val = 1;

            char cancel_svid[20];

            strcpy(cancel_svid, args[1]);
            
            put_result = m->putUnique(m, cancel_svid, (void*)&cancel_val);

            
            if(!put_result){
                //printf("key already exists, putUnique has failed\n");
            }
            else{
                //printf("added key: %s to cancelmap with value 1\n", cancel_svid);
            }
            

            sprintf(response, "1%08lu", svid);
        }
        else{
            //did not have a legal first word AND the correct number of arguments
            sprintf(response, "0%08lu", svid);
        }
        
        rlen = strlen(response) + 1;
        assert(bxp_response(svc, &ep, response, rlen));
    }

    return NULL;
}


int cmpEvents(void *p1, void *p2){
    // need to compare the time (seconds + microseconds) of p1 and p2
    /*
    unsigned long long *time1p = (unsigned long long *)p1;
    unsigned long long *time2p = (unsigned long long *)p2;

    unsigned long long time1 = *time1p;
    unsigned long long time2 = *time2p;

    int c;

    if(time1 < time2){
        c = -1;
    }
    else if(time1 > time2){
        c = 1;
    }
    else{
        c = 0;
    }
    */

    
    struct timeval *tv1 = (struct timeval *)p1;
    struct timeval *tv2 = (struct timeval *)p2;

    int c;
    if((tv1->tv_sec) < (tv2->tv_sec)){
        c = -1;
    }
    else if((tv1->tv_sec) > (tv2->tv_sec)){
        c = 1;
    }
    else if((tv1->tv_sec) == (tv2->tv_sec)){
        if((tv1->tv_usec) < (tv2->tv_usec)){
            c = -1;
        }
        else if((tv1->tv_usec) > (tv2->tv_usec)){
            c = 1;
        }
        else{
            c = 0;
        }
    }
    
    return c;
}

int main(UNUSED int argc, UNUSED char *argv[]){
    pthread_t prioqueueThread;
    pthread_t timerThread;
    //pthread_t qprocessThread;
    //pthread_t qinsertionThread;

    pq = PrioQueue_create(cmpEvents, doNothing, doNothing);
    q = Queue_create(doNothing);
    rep_q = Queue_create(doNothing);
    m = CSKMap_create(doNothing);

    int prioqT = pthread_create(&prioqueueThread, NULL, &receiver_fxn, NULL);
    int timerT = pthread_create(&timerThread, NULL, &timer, NULL);

    if(prioqT != 0){
        fprintf(stderr, "Failed to create priority queue insertion thread\n");
    }
    else{
        //printf("prioqueue thread created\n");
        pthread_join(prioqueueThread, NULL);
    }
    
    if(timerT != 0){
        fprintf(stderr, "Failed to create timer thread\n");
    }
    else{
        //printf("timer thread created\n");
        pthread_join(timerThread, NULL);
    }

    return 0;
}

/*
Name: Nathaniel Mason
DuckID: nmason
Assignment Title: CIS 415 Project 3
This is my own work
*/

#include "BXP/bxp.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <valgrind/valgrind.h>

#define UNUSED __attribute__((unused))
#define HOST "localhost" 
#define PORT 19999
#define SERVICE "DTS"

int extractWords(char *buf, char *sep, char *words[]) {
    int i;
    char *p;

    for (p = strtok(buf, sep), i = 0; p != NULL; p = strtok(NULL, sep),i++)
        words[i] = p;
    words[i] = NULL;
    return i;
}

void *receiver_fxn () {
    BXPEndpoint ep;
    BXPService svc;
    
    int ifEncrypt = 1;
    char query[10000], response[10001];
    unsigned qlen, rlen;

    assert(bxp_init(PORT, ifEncrypt));
    assert((svc = bxp_offer(SERVICE)));

    //check the request received for legal strings and number of arguments
    //Legal queries have secitons split using | so can split each string by the | character
    char *delim = "|";
    //char *qtoken;
    int one_shot_req;
    int repeat_req;
    int cancel_req;
    int num_args_needed;
    int num_args_satisfied;
    int num_args;
    char *word1 = "OneShot";
    char *word2 = "Repeat";
    char *word3 = "Cancel";
    char query_to_split[10000];

    char *args[10];
    
    while ((qlen = bxp_query(svc, &ep, query, 10000)) > 0) {
        //printf("Query: %s\n", query);
        one_shot_req = 0;
        repeat_req = 0;
        cancel_req = 0;
        num_args = 0; 
        num_args_satisfied = 0;

        strcpy(query_to_split, query);
        //printf("query to split: %s\n", query_to_split);

        num_args = extractWords(query_to_split, delim, args);

        //printf("num args: %d\n", num_args);
        /*
        for(int i = 0; i < num_args; i++){
            printf("arg %d: %s\n", i, args[i]);
        }
        */

        char *first_word = args[0];
        
        if(strcmp(first_word, word1) == 0){
            one_shot_req = 1;
        }
        if(strcmp(first_word, word2) == 0){
            repeat_req = 1;
        }

        if(strcmp(first_word, word3) == 0){
            cancel_req = 1;
        }

        if(one_shot_req || repeat_req){
            num_args_needed = 7;
        }
        else if(cancel_req){
            num_args_needed = 2;
        }

        if(num_args == num_args_needed){
            num_args_satisfied = 1;
        }

        if((one_shot_req || repeat_req || cancel_req) && num_args_satisfied){
            sprintf(response, "1%s", query); //1 means a successful request, so on client side will echo the request back
        }
        else{
            sprintf(response, "0%s", query); //did not have a legal first word AND the correct number of arguments
        }
        
        rlen = strlen(response) + 1;
        assert(bxp_response(svc, &ep, response, rlen));

        VALGRIND_MONITOR_COMMAND("leak_check summary");
    }

    return NULL;
}

int main(UNUSED int argc, UNUSED char *argv[]){
    
    pthread_t receiverThread;

    int receiverT = pthread_create(&receiverThread, NULL, &receiver_fxn, NULL);

    if(!receiverT){
        pthread_join(receiverThread, NULL);
    }
    else{
        printf("Error: Failed to create receiver Thread\n");
    }

    return 0;
}

/*
Name: Nathaniel Mason
DuckID: nmason
Assignment Title: CIS 415 Project 3
This is my own work
*/

#include "BXP/bxp.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <valgrind/valgrind.h>

#define UNUSED __attribute__((unused))
#define HOST "localhost" 
#define PORT 19999
#define SERVICE "DTS"

void *receiver_fxn () {
    BXPEndpoint ep;
    BXPService svc;
    
    int ifEncrypt = 1;
    char query[10000], response[10001];
    unsigned qlen, rlen;

    assert(bxp_init(PORT, ifEncrypt));
    assert((svc = bxp_offer(SERVICE)));

    while ((qlen = bxp_query(svc, &ep, query, 10000)) > 0) {
        sprintf(response, "1%s", query); //1 means a successful request, so on client side will echo the request back
        rlen = strlen(response) + 1;
        assert(bxp_response(svc, &ep, response, rlen));

        VALGRIND_MONITOR_COMMAND("leak_check summary");
    }
    

    return NULL;
}

int main(UNUSED int argc, UNUSED char *argv[]){
    
    pthread_t receiverThread;

    int receiverT = pthread_create(&receiverThread, NULL, &receiver_fxn, NULL);

    if(!receiverT){
        pthread_join(receiverThread, NULL);
    }
    else{
        printf("Error: Failed to create receiver Thread\n");
    }

    return 0;
}
